output "alb_hostname" {
  value = aws_alb.main.dns_name
}

output "s3_bucket_name" {
  value = aws_s3_bucket.www.id
}

output "s3_bucket_region" {
  value = aws_s3_bucket.www.region
}
output "s3_bucket_endpoint" {
  value = aws_s3_bucket.www.website_endpoint
}

output "acm_certificate_arn" {
  description = "arn of acm certificate"
  value       = aws_acm_certificate.cert.arn
}

output "aws_route53_zone_arn" {
  description = "arn route53 arn"
  value       = aws_route53_zone.primary.arn
}

# output "aws_route53_record_arn" {
#   description = "arn terraform record route53"
#   value       = aws_route53_record.www.id
# }
