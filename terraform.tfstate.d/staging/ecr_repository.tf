
/*
  Not used (really), trying to sync ECR with Gitlab Repository on every
  push to main create a new registry of that container and then update that image
  on Falgate
*/

resource "aws_ecr_repository" "my-app-api" {
  name                 = "my-app-api"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}