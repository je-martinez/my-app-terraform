/*
  Just to specify AWS Region
*/

provider "aws" {
  region = var.aws_region
}