# My App Terraform

In order to test this infrastructure on AWS, is necessary do a few steps first.

1. Because we are going to use Route53 with a Custom Domain is necessary have a domain first, you can buy it or you get one for free on https://www.freenom.com

2. Go to the folder **terraform.tfstate.d > stagging** and open your console, here's where we are going to run all our commands using terraform.

3. At this point you have already a domain that you bought or got for free, go to the file `variables.tf` you would see two variables that's important to change, `freenom_domain` and `www_freenom_domain`, replace the current value `my-app-aws.tk` and `www.my-app-aws.tk`, with your custom domain.

4. Now it's all set, get can start running commands, but first let's comment out all the lines of the file `cloudfront.tf` that's because we don't have configure yet our domain related with route53, we can do this later. Now me can run:

```bash
terraform run
terraform appy
```

5. Now it's important to configure our domain related to the new record of route53 generated on previous steps. Do to that go to the section of **Route53** on your AWS Console, the record would match with the name of your domain, now copy the **Nameservers (NS)** and put these on your domain configuration, usually would take a few minutes complete the DNS Propagation (15 - 20min), you can check if is completed right here https://dnschecker.org/, if you are using Freenom you can follow this guide:

**[ESP/SPA]** https://www.youtube.com/watch?v=LpHFSE3IEVE&ab_channel=SySCursos

**[ENG]** https://www.youtube.com/watch?v=IjcHp94Hq8A&ab_channel=TheCloudBook

6. Now it's almost set, now we can run the remove all comments on the `cloudfront.tf` and run again:

```bash
terraform run
terraform appy
```
