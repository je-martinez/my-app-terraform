/*
  Create a route 53 zone for managing our custom domain via Amazon 
  all this is base on the next documentation:
  https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_zone
*/

resource "aws_route53_zone" "primary" {
  name = var.freenom_domain
}

/*
  Just for DNS Validation of our custom domain or ACM Certification Validation this is used
  together with our aws_route53_zone
  all this is based on the next documentation:
  https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record
  https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/acm_certificate_validation
*/
resource "aws_route53_record" "www" {
  for_each = {
    for dvo in aws_acm_certificate.cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = aws_route53_zone.primary.zone_id
}