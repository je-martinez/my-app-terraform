/*
  Create Application Load Balancer for incoming traffic using
  all the subnet that we are going to create. On this part in a combination
  with multiples demos and examples from Youtube.
*/

resource "aws_alb" "main" {
  name               = "myapp-load-balancer"
  internal           = false
  load_balancer_type = "application"
  subnets            = aws_subnet.public.*.id
  security_groups = [
    aws_security_group.http.id,
    aws_security_group.egress_all.id,
  ]
  depends_on = [aws_internet_gateway.gw]
}

resource "aws_alb_target_group" "app" {
  name        = "myapp-target-group"
  port        = 3000
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = aws_vpc.main.id

  health_check {
    enabled = true
    path    = "/health"
  }
  depends_on = [
    aws_alb.main
  ]
}

resource "aws_alb_listener" "my_api_http" {
  load_balancer_arn = aws_alb.main.arn
  port              = var.app_port
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.app.arn
    type             = "forward"
  }
}