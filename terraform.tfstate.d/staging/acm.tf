/*
  Create an AWS ACM Certficate with our domain 
  this is base on the next documentation:
  https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/acm_certificate
*/

resource "aws_acm_certificate" "cert" {
  domain_name               = "*.${var.freenom_domain}"
  validation_method         = "DNS"
  subject_alternative_names = ["${var.freenom_domain}"]
}